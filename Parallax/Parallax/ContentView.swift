//
//  ContentView.swift
//  Parallax
//
//  Created by Vitaliy on 21.10.2020.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



struct HomeView: View {
    @State var rating = 0
    var body: some View {
        ScrollView(.vertical, showsIndicators: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/) {
            GeometryReader {reader in
                Image("main")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .offset(y: -reader.frame(in: .global).minY)
                    .frame(width: UIScreen.main.bounds.width, height: reader.frame(in: .global).minY > 0 ? reader.frame(in: .global).minY + 480 : 480)
            }
            .frame(height: 480)
            
            
            VStack(alignment: .leading, spacing: 15) {
                Text("Toys Story")
                    .font(.system(size: 35, weight: .bold))
                    .foregroundColor(.white)
                
                HStack(spacing: 15) {
                    ForEach(1...5, id: \.self) {i in
                        Button(action: {
                            self.rating = i
                        }) {
                            Image(systemName: "star.fill")
                                .foregroundColor(self.rating >= i ? .yellow : .white)
                        }
                    }
                }
                
                Text("Some Scene May Scare Very Young Childrens")
                    .font(.caption)
                    .foregroundColor(.white)
                    .padding(.top, 5)
                
                Text(lorem)
                    .padding(.top, 10)
                    .foregroundColor(.white)
                
                HStack(spacing: 15) {
                    Button(action: {}, label: {
                        Text("Bookmark")
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .background(Color.blue)
                            .cornerRadius(10)
                        
                    })
                    Button(action: {}, label: {
                        Text("Buy Tickets")
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .background(Color.red)
                            .cornerRadius(10)
                        
                    })
                }
                .padding(.top, 25)
            }
            
            .padding(.top, 25)
            .padding(.horizontal)
            .background(Color.black)
            .cornerRadius(20)
            .offset(y: -35)
        }
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        .background(Color.black.edgesIgnoringSafeArea(.all))
    }
}


var lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
